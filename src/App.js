import React from 'react';
import Home from './components/home/home';
import './assets/styles/main.scss'
import './App.css'
function App() {
  return (
    <React.Fragment >
      <main className="container">
        <Home/>
      </main>
    </React.Fragment>
  );
}

export default App;
