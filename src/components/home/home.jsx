import React, { Component } from 'react'
import './home.scss'
import winner from '../../assets/images/_1.jpg';
import failer from '../../assets/images/_2.jpg'
export default class Home extends Component {
    render() {
        return (
            <div className="row">
                <div className="col">
            <div className="border border-raduis-40 border-gray-1 max-300 mtb-20">
                
                <div className="d-flex align-items-center border-bottom border-gray-2">
                    <img src={winner} alt="" className="w-100 rounded-circle ms-4"/>
                    <div className="text-gray-2 size-2 ms-4 weight-700">Petey Cruiser</div> 
                    <div className="bg-error weight-900 h-80 max-70 w-100 ml-auto text-white weight-800 size-20 d-flex align-items-center justify-content-center border-top-end-raduis-40 up">
                        1
                    </div>
                </div>
                
                <div className="d-flex align-items-center">
                    <img src={failer} alt="" className="w-100 rounded-circle ms-4"/>
                    <div className="text-gray-2 size-2 ms-4 weight-700">Bob Frappies</div>
                    <div className="bg-primary weight-900 h-80 max-70 w-100 ml-auto text-white weight-800 size-20 d-flex align-items-center justify-content-center border-bottom-end-raduis-40 down">
                        2
                    </div>
                </div>

                {/* <div className="text-gray-2 size-2 position-absolute">final</div>
                <div className="text-gray-2 size-2 position-absolute">20/7/4 10:30</div> */}
            </div>
            <div className="border border-raduis-40 border-gray-1 max-300 mtb-20">
                
                <div className="d-flex align-items-center border-bottom border-gray-2">
                    <img src={winner} alt="" className="w-100 rounded-circle ms-4"/>
                    <div className="text-gray-2 size-2 ms-4 weight-700">Petey Cruiser</div> 
                    <div className="bg-error weight-900 h-80 max-70 w-100 ml-auto text-white weight-800 size-20 d-flex align-items-center justify-content-center border-top-end-raduis-40 up">
                        1
                    </div>
                </div>
                
                <div className="d-flex align-items-center">
                    <img src={failer} alt="" className="w-100 rounded-circle ms-4"/>
                    <div className="text-gray-2 size-2 ms-4 weight-700">Bob Frappies</div>
                    <div className="bg-primary weight-900 h-80 max-70 w-100 ml-auto text-white weight-800 size-20 d-flex align-items-center justify-content-center border-bottom-end-raduis-40 down">
                        2
                    </div>
                </div>

                {/* <div className="text-gray-2 size-2 position-absolute">final</div>
                <div className="text-gray-2 size-2 position-absolute">20/7/4 10:30</div> */}
            </div>
            <div className="border border-raduis-40 border-gray-1 max-300 mtb-20">
                
                <div className="d-flex align-items-center border-bottom border-gray-2">
                    <img src={winner} alt="" className="w-100 rounded-circle ms-4"/>
                    <div className="text-gray-2 size-2 ms-4 weight-700">Petey Cruiser</div> 
                    <div className="bg-error weight-900 h-80 max-70 w-100 ml-auto text-white weight-800 size-20 d-flex align-items-center justify-content-center border-top-end-raduis-40 up">
                        1
                    </div>
                </div>
                
                <div className="d-flex align-items-center">
                    <img src={failer} alt="" className="w-100 rounded-circle ms-4"/>
                    <div className="text-gray-2 size-2 ms-4 weight-700">Bob Frappies</div>
                    <div className="bg-primary weight-900 h-80 max-70 w-100 ml-auto text-white weight-800 size-20 d-flex align-items-center justify-content-center border-bottom-end-raduis-40 down">
                        2
                    </div>
                </div>

                {/* <div className="text-gray-2 size-2 position-absolute">final</div>
                <div className="text-gray-2 size-2 position-absolute">20/7/4 10:30</div> */}
            </div>
            <div className="border border-raduis-40 border-gray-1 max-300 mtb-20">
                
                <div className="d-flex align-items-center border-bottom border-gray-2">
                    <img src={winner} alt="" className="w-100 rounded-circle ms-4"/>
                    <div className="text-gray-2 size-2 ms-4 weight-700">Petey Cruiser</div> 
                    <div className="bg-error weight-900 h-80 max-70 w-100 ml-auto text-white weight-800 size-20 d-flex align-items-center justify-content-center border-top-end-raduis-40 up">
                        1
                    </div>
                </div>
                
                <div className="d-flex align-items-center">
                    <img src={failer} alt="" className="w-100 rounded-circle ms-4"/>
                    <div className="text-gray-2 size-2 ms-4 weight-700">Bob Frappies</div>
                    <div className="bg-primary weight-900 h-80 max-70 w-100 ml-auto text-white weight-800 size-20 d-flex align-items-center justify-content-center border-bottom-end-raduis-40 down">
                        2
                    </div>
                </div>

                {/* <div className="text-gray-2 size-2 position-absolute">final</div>
                <div className="text-gray-2 size-2 position-absolute">20/7/4 10:30</div> */}
            </div>
    
            </div>
                
            <div className="col">
            <div className="border border-raduis-40 border-gray-1 max-300 mtb-20">
                
                <div className="d-flex align-items-center border-bottom border-gray-2">
                    <img src={winner} alt="" className="w-100 rounded-circle ms-4"/>
                    <div className="text-gray-2 size-2 ms-4 weight-700">Petey Cruiser</div> 
                    <div className="bg-error weight-900 h-80 max-70 w-100 ml-auto text-white weight-800 size-20 d-flex align-items-center justify-content-center border-top-end-raduis-40 up">
                        1
                    </div>
                </div>
                
                <div className="d-flex align-items-center">
                    <img src={failer} alt="" className="w-100 rounded-circle ms-4"/>
                    <div className="text-gray-2 size-2 ms-4 weight-700">Bob Frappies</div>
                    <div className="bg-primary weight-900 h-80 max-70 w-100 ml-auto text-white weight-800 size-20 d-flex align-items-center justify-content-center border-bottom-end-raduis-40 down">
                        2
                    </div>
                </div>

                {/* <div className="text-gray-2 size-2 position-absolute">final</div>
                <div className="text-gray-2 size-2 position-absolute">20/7/4 10:30</div> */}
            </div>
            <div className="border border-raduis-40 border-gray-1 max-300 mtb-20">
                
                <div className="d-flex align-items-center border-bottom border-gray-2">
                    <img src={winner} alt="" className="w-100 rounded-circle ms-4"/>
                    <div className="text-gray-2 size-2 ms-4 weight-700">Petey Cruiser</div> 
                    <div className="bg-error weight-900 h-80 max-70 w-100 ml-auto text-white weight-800 size-20 d-flex align-items-center justify-content-center border-top-end-raduis-40 up">
                        1
                    </div>
                </div>
                
                <div className="d-flex align-items-center">
                    <img src={failer} alt="" className="w-100 rounded-circle ms-4"/>
                    <div className="text-gray-2 size-2 ms-4 weight-700">Bob Frappies</div>
                    <div className="bg-primary weight-900 h-80 max-70 w-100 ml-auto text-white weight-800 size-20 d-flex align-items-center justify-content-center border-bottom-end-raduis-40 down">
                        2
                    </div>
                </div>

                {/* <div className="text-gray-2 size-2 position-absolute">final</div>
                <div className="text-gray-2 size-2 position-absolute">20/7/4 10:30</div> */}
            </div>
            </div>
            <div className="col"> <div className="border border-raduis-40 border-gray-1 max-300 mtb-20">
                
                <div className="d-flex align-items-center border-bottom border-gray-2">
                    <img src={winner} alt="" className="w-100 rounded-circle ms-4"/>
                    <div className="text-gray-2 size-2 ms-4 weight-700">Petey Cruiser</div> 
                    <div className="bg-error weight-900 h-80 max-70 w-100 ml-auto text-white weight-800 size-20 d-flex align-items-center justify-content-center border-top-end-raduis-40 up">
                        1
                    </div>
                </div>
                
                <div className="d-flex align-items-center">
                    <img src={failer} alt="" className="w-100 rounded-circle ms-4"/>
                    <div className="text-gray-2 size-2 ms-4 weight-700">Bob Frappies</div>
                    <div className="bg-primary weight-900 h-80 max-70 w-100 ml-auto text-white weight-800 size-20 d-flex align-items-center justify-content-center border-bottom-end-raduis-40 down">
                        2
                    </div>
                </div>

                {/* <div className="text-gray-2 size-2 position-absolute">final</div>
                <div className="text-gray-2 size-2 position-absolute">20/7/4 10:30</div> */}
            </div></div>
            </div>
            
        )
    }
}
